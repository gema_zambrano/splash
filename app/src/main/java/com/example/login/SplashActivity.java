package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.login.ui.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    static int TIMEOUT_MILLIS=10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide(); // ocultar barra de arriba del telefono
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i =new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        },TIMEOUT_MILLIS);
    }
}
